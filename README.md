# NR - Net Radar

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

## Simple local IP network browser

**It runs on my macOS and FreeBSD ¯\_(ツ)_/¯ Should work on Linux too**

**WARNING!** This is Alpha version. Early development stage!

### Requirements
* root privileges to run
* Python 3
* netifaces module
* zeroconf module
* System utilities and external tools like ftp, FUSE sshfs, showmount, smbutil and etc are required to access remote 
resources.  

**WARNING!** Always umount remote filesystems mounted under nr_root directory or you may occasionally lose the data!

### Changelog
```
2018.10.15    v0.1  Mikhail Zakharov <zmey20000@yahoo.com>
              Initial release, LICENSE, README.md
2018.10.20    v0.2  Mikhail Zakharov <zmey20000@yahoo.com>
              TCP port-scanner added
2018.11.03    v0.3  Mikhail Zakharov <zmey20000@yahoo.com>
              First port-action script added
2018.11.25    v 0.4   Mikhail Zakharov <zmey20000@yahoo.com>
              The daemon-mode draft
2018.12.02    v 0.5   Mikhail Zakharov <zmey20000@yahoo.com>
               Port-action scripts enhancement: "write a file" and "run a command" actions
2018.12.28    v 0.6   Mikhail Zakharov <zmey20000@yahoo.com>
              Zeroconf network scan introduction

```
