#!/usr/bin/env python3

# Copyright (c) 2018 Mikhail Zakharov <zmey20000@yahoo.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

#
# NR - Net Radar - Simple local IP network browser
#

# It runs on my macOS and FreeBSD. Should work on Linux too
# Requires root privileges

# WARNING! Alpha version. Early development stage!
# WARNING! Always umount remote filesystems mounted under nr_root directory or you may occasionally lose the data!

# 2018.10.15    v 0.1   Mikhail Zakharov <zmey20000@yahoo.com>
#               Initial release, LICENSE, README.md
# 2018.10.20    v 0.2   Mikhail Zakharov <zmey20000@yahoo.com>
#               TCP port-scanner added
# 2018.11.03    v 0.3   Mikhail Zakharov <zmey20000@yahoo.com>
#               The first port-action script added
# 2018.11.25    v 0.4   Mikhail Zakharov <zmey20000@yahoo.com>
#               The daemon-mode draft
# 2018.12.02    v 0.5   Mikhail Zakharov <zmey20000@yahoo.com>
#               Port-action scripts enhancement: "write a file" and "run a command" actions
# 2018.12.28    v 0.6   Mikhail Zakharov <zmey20000@yahoo.com>
#               Zeroconf network scan introduction


import sys
import resource

import netifaces
import ipaddress
import socket

import threading
import subprocess

import time

import pathlib
import shutil

import json

import nr_config
import utils
import ping
import portscan
import zero_conf

import nr_hpaction                                              # Define actions for host:port connection scripts


nr_version = '0.6'


def af_conv(address_family='AF_INET'):
    """Convert Address family to IP protocol version number and back"""

    if address_family == 'AF_INET':
        return 4
    elif address_family == 'AF_INET6':
        return 6
    elif address_family == 4:
        return 'AF_INET'
    elif address_family == 6:
        return 'AF_INET6'
    else:
        utils.log_message('af_conv(): Wrong address family: {}'.format(address_family), 1)
        return None


def get_myifs(ifblacklist=()):
    """All local network interfaces excluding blacklisted ones"""

    ifaces = []
    for iface in netifaces.interfaces():
        if iface not in ifblacklist:
            ifaces.append(iface)
        else:
            utils.log_message('Ignoring blacklisted interface: {}'.format(iface), 3)

    # Returns: ['lo0', 'gif0', 'stf0', 'XHC20', 'en0', 'p2p0', 'awdl0', 'en1', 'bridge0', 'utun0', 'utun1']
    return ifaces


def get_myifmacs(iface):
    """Get MAC-addresses on the interface"""

    interface_block = netifaces.ifaddresses(iface)                              # Interface data block
    for af_number in interface_block:                                           # Address family number
        addr_family = netifaces.address_families[af_number]                     # Address family mnemonic: AF_SOMETHING

        if addr_family == 'AF_LINK':
            addr = []
            for addr_block in interface_block[af_number]:                       # Address block
                addr.append(addr_block['addr'])

                return ', '.join(addr)
    # Returns: 'c2:5e:b2:07:b0:0a' or None
    return None


def get_myifnets(iface, netlacklist=()):
    """Get networks from the interface"""

    addr = {}

    interface_block = netifaces.ifaddresses(iface)                              # Interface data block
    for af_number in interface_block:                                           # Address family number
        addr_family = netifaces.address_families[af_number]                     # Address family mnemonic: AF_SOMETHING

        if 'AF_INET' in addr_family:
            for addr_block in interface_block[af_number]:                       # Address block
                myip = addr_block['addr']
                network = ip2net(myip, addr_block['netmask'], addr_family)
                if network.compressed not in netlacklist:
                    addr[network.compressed] = {
                        'attributes': {
                            'myip': myip,
                            'netmask': addr_block['netmask'],
                            'family': addr_family,
                            'broadcast': network.broadcast_address.compressed,
                            'num_addresses': network.num_addresses,
                            'atime': int(time.time())
                        }
                    }
                else:
                    utils.log_message('Ignoring blacklisted network: {}'.format(network.compressed), 3)
    # Returns:
    # {
    #   '192.168.1.3': {
    #       'attributes': {
    #           'myip': '192.168.1.3', 'netmask': '255.255.255.0', 'family': 'AF_INET', 'atime': 1536607367
    #       }
    #   }
    # }
    return addr


def ip2net(ip_addr, ip_mask, address_family='AF_INET'):
    """Calculate network address based on IP, mask and protocol version"""

    if not ip_addr and not ip_mask:
        utils.log_message('ip2net() requires two arguments to run', 1)
        return None

    if address_family == 'AF_INET':
        ip_full = ip_addr.split('/')[0] + '/' + ip_mask
        ip_net = ipaddress.IPv4Network(ip_full, strict=False)
    elif address_family == 'AF_INET6':
        ip_full = ip_addr.split('%')[0] + '/' + ip_mask.split('/')[1]
        ip_net = ipaddress.IPv6Network(ip_full, strict=False)
    else:
        utils.log_message('ip2net(): Wrong address family: {}'.format(address_family), 1)
        return None

    utils.log_message('Calculated Network address: {net} for IP: {ip}'.format(net=ip_net, ip=ip_full), 4)
    return ip_net


def ping_ipaddress(ip_addr, a_family='AF_INET'):
    """Ping a single IPv4 or IPv6 address. Return 1 if host is up, 0 if down

    It's a wrapper for ping.ping() function with several predefined common arguments"""

    host = {}
    ping.ping(host, ip_addr, source_host=None, interface=None, a_family=a_family, unicast=True, sequence=0, ttl=64)
    if host:
        return 1                                                    # The host is alive
    return 0                                                        # The host is dead


def hosts2results(result, hosts):
    """Convert hosts list to result dictionary and add attributes"""

    for host in hosts:                                              # Host is IP actually
        result[host] = {}
        result[host]['attributes'] = {}
        result[host]['attributes']['atime'] = int(time.time())
        result[host]['attributes']['ip'] = host
        try:
            result[host]['attributes']['name'] = socket.gethostbyaddr(host)[0]
        except socket.herror:
            result[host]['attributes']['name'] = host               # Save IP instead of name


def ping_ipnetwork(ip_addr, address_family='AF_INET', iface=''):
    """IPv4/v6 network ping. IP address must include netmask"""

    hosts = []
    host_threads = []
    result = {}

    if address_family != 'AF_INET6':
        network = ipaddress.IPv4Network(ip_addr, strict=False)
    else:
        network = ipaddress.IPv6Network(ip_addr, strict=False)

    utils.log_message('Ping network: {}'.format(network.network_address), 3)

    for ip, cnt in zip(network, range(0, network.num_addresses)):
        if ip != network.network_address and ip != network.broadcast_address or network.num_addresses == 1:
            host_threads.append(threading.Thread(target=ping.ping,
                                                 args=(hosts, ip.compressed, None, iface, address_family, True, cnt)))

    for thr in host_threads:
        thr.start()
        while (len(threading.enumerate())) > nr_config.nr_threads_max:
            utils.log_message('Waiting for threads to finish', 4)
            time.sleep(1)

    # Wait for all threads to finish
    for thr in host_threads:
        thr.join()

    hosts2results(result, hosts)
    utils.log_message('Alive hosts: {}'.format(result), 3)
    return result


def ping_ipnetwork4(ip_addr, iface=''):
    """IPv4 network ping. IP address must include netmask.
    It's a wrapper for ping_ipnetwork() function for IPv4"""

    return ping_ipnetwork(ip_addr, address_family='AF_INET', iface=iface)


def ping_ipnetwork6(ip_addr, iface=''):
    """IPv6 network ping. IP address must include netmask.
    It's a wrapper for ping_ipnetwork() function for IPv6"""

    return ping_ipnetwork(ip_addr,  address_family='AF_INET6', iface=iface)


def mping_ipnetwork4(ip_broadcast):
    """Broadcast IPv4 ping"""

    hosts = {}

    utils.log_message('Broadcast ping: {}'.format(ip_broadcast), 3)
    hosts = ping.ping(hosts, ip_broadcast, source_host=None, unicast=False)
    utils.log_message('Alive hosts: {}'.format(hosts), 3)
    return hosts


def mping_ipnetwork6(source_ip, iface):
    """Multicast IPv6 ping. Source IP must include netmask"""

    hosts = []
    result = {}

    utils.log_message('Multicast ping: {}'.format(source_ip), 3)
    ping.ping(hosts, 'ff02::1', source_host=source_ip, interface=iface, a_family='AF_INET6', unicast=False)

    hosts2results(result, hosts)
    utils.log_message('Alive hosts: {}'.format(result), 3)
    return result


def pscan_ipnetwork(ip_network):
    """Scan the list of IPs in the network for open ports"""

    for ip in ip_network['addresses']:
        ip_network['addresses'][ip]['ports'] = dict.fromkeys(
            portscan.pscan_ipaddress(ip, a_family=ip_network['attributes']['family']),
            {'attributes': {'atime': int(time.time())}})


def get_myifvirtual(vnets=nr_config.nr_virtual_networks):
    """Return a template virtual network interface with networks"""

    ifv = dict()
    ifv['attributes'] = {}
    ifv['attributes']['mac'] = ''
    ifv['attributes']['atime'] = int(time.time())

    ifv['networks'] = {}
    for vnet in vnets:
        ifv['networks'][vnet] = {}
        ifv['networks'][vnet]['attributes'] = {}
        ifv['networks'][vnet]['addresses'] = {}

    return ifv


def create_nrmap(ignore_list):
    """Create NR main map """

    nrm = {}

    interfaces = get_myifs(ignore_list['interfaces'])

    for interface in interfaces:
        utils.log_message('Discovered interface: {i}'.format(i=interface), 3)

        nrm[interface] = {}
        nrm[interface]['attributes'] = {}
        nrm[interface]['attributes']['mac'] = get_myifmacs(interface)
        nrm[interface]['attributes']['atime'] = int(time.time())
        nrm[interface]['networks'] = get_myifnets(interface, ignore_list['networks'])

        for network in nrm[interface]['networks']:
            utils.log_message('Interface: {i}, processing network: {n}'.format(i=interface, n=network), 3)

            net = nrm[interface]['networks'][network]
            if net['attributes']['family'] == 'AF_INET':
                if net['attributes']['num_addresses'] <= nr_config.nr_ping_net_hosts_max:
                    net['addresses'] = ping_ipnetwork4(network)
                else:
                    net['addresses'] = mping_ipnetwork4(net['attributes']['broadcast'])
                pscan_ipnetwork(net)
            if net['attributes']['family'] == 'AF_INET6':
                if '%' in net['attributes']['myip'] or \
                        net['attributes']['num_addresses'] > nr_config.nr_ping_net_hosts_max:
                    net['addresses'] = mping_ipnetwork6(net['attributes']['myip'], interface)
                else:
                    net['addresses'] = ping_ipnetwork6(net['attributes']['myip'], interface)
                pscan_ipnetwork(net)

    nrm[nr_config.nr_virtual_interface] = get_myifvirtual()
    for vnet in nr_config.nr_virtual_networks:
        nrm[nr_config.nr_virtual_interface]['networks'][vnet] = zero_conf.get_zc_network()
        utils.log_message('Zeroconf network found on the interface: {iface}: {znet}'.
                          format(iface=nr_config.nr_virtual_interface,
                                 znet=format(nrm[nr_config.nr_virtual_interface]['networks'][vnet])), 3)
    return nrm


def fs_path_update(p_node, action='mk', attributes=''):
    """Update paths under the nr_root directory"""

    if action == 'mk':
        p_node.mkdir(mode=nr_config.port_action_filemode, parents=True, exist_ok=True)
        shutil.chown(p_node, user=nr_config.port_action_fileowner, group=nr_config.port_action_filegroup)
        if attributes:
            path_attribs = pathlib.Path(p_node, nr_config.nr_attributes)
            path_attribs.write_text(json.dumps(attributes, indent=4))
            shutil.chown(path_attribs, user=nr_config.port_action_fileowner, group=nr_config.port_action_filegroup)
            path_attribs.chmod(nr_config.port_action_filemode)
    else:
        try:
            shutil.rmtree(p_node)
        except FileNotFoundError:
            utils.log_message('fs_path_update(): Unable to remove {}'.format(p_node), 2)


def port_action(p_node, global_attributes, port):
    """Port-action script processing"""

    utils.log_message('Creating port action for {host}:{port}'.format(host=global_attributes['name'], port=port), 4)

    if global_attributes['name'] in nr_hpaction.hp_action:
        target_host = global_attributes['name']
    elif global_attributes['ip'] in nr_hpaction.hp_action:
        target_host = global_attributes['ip']
    else:
        target_host = 'default'

    if port in nr_hpaction.hp_action[target_host]:
        for action in nr_hpaction.hp_action[target_host][port]:
            for action_type in nr_hpaction.hp_action[target_host][port][action]:
                if action_type.lower() == 'file':
                    # Write a text file, typically save a shell script
                    action_path = pathlib.Path(p_node, action)
                    action_path.write_text(nr_hpaction.hp_action[target_host][port][action][action_type].
                                           format(target=global_attributes['ip']))
                    shutil.chown(action_path,
                                 user=nr_config.port_action_fileowner, group=nr_config.port_action_filegroup)
                    action_path.chmod(nr_config.port_action_filemode)
                    utils.log_message('Written port-action script file: {}'.format(action_path), 4)
                    break
                elif action_type.lower() == 'command':
                    # Execute commands that are defined in the nr_action.py file under the right port
                    cmd = nr_hpaction.hp_action[target_host][port][action][action_type].\
                        format(target=global_attributes['ip'], cwd=p_node,
                               user=nr_config.port_action_fileowner,
                               group=nr_config.port_action_filegroup,
                               mode='{:o}'.format(nr_config.port_action_filemode))
                    retval = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    utils.log_message('The command: {cmd} returned: [{rv}]'.
                                      format(cmd=cmd, rv=retval.stdout.decode('utf-8')), 2)
                    break
                else:
                    utils.log_message('Ignoring unknown port: {p} action: {a} type: {t}'.
                                      format(p=port, a=action, t=action_type), 2)


def diff_nrmap(new_nrmap, old_nrmap, path_root=nr_config.nr_root, action='mk'):
    """Find elements that are exist in new_nrmap, but not in old_nrmap"""

    if action.lower() != 'mk' and action.lower() != 'rm':
        utils.log_message('fs_path_update() wrong action: {act}'.format(act=action), 1)
        sys.exit(2)

    # Interfaces
    for n_iface in new_nrmap:
        if not old_nrmap or n_iface not in old_nrmap:
            # A new interface is discovered
            utils.log_message('Identified element to {act}: {int}'.format(int=n_iface, act=action), 3)
            path_node = pathlib.Path(path_root, n_iface)
            attribs = new_nrmap[n_iface]['attributes']
            fs_path_update(path_node, action=action, attributes=attribs)

            # Networks on the new interface
            for n_net in new_nrmap[n_iface]['networks']:
                utils.log_message('Identified element to {act}: {int}->{net}'.
                                  format(int=n_iface, net=n_net, act=action), 3)
                path_node = pathlib.Path(path_root, n_iface, n_net.replace('/', '_'))
                attribs = new_nrmap[n_iface]['networks'][n_net]['attributes']
                fs_path_update(path_node, action=action, attributes=attribs)

                # IP addresses in the new network
                for n_ip in new_nrmap[n_iface]['networks'][n_net]['addresses']:
                    utils.log_message('Identified element to {act}: {int}->{net}->{ip}'.
                                      format(int=n_iface, net=n_net, ip=n_ip, act=action), 3)
                    ip_attribs = new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['attributes']
                    n_name = ip_attribs['name']
                    if 'ip' not in ip_attribs:
                        # An awkward patch for IP address in the ip_attribs
                        ip_attribs['ip'] = n_ip
                    path_node = pathlib.Path(path_root, n_iface, n_net.replace('/', '_'), n_name)
                    fs_path_update(path_node, action=action, attributes=ip_attribs)

                    # Open ports of the host
                    for n_port in new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['ports']:
                        path_node = pathlib.Path(path_root, n_iface, n_net.replace('/', '_'), n_name, str(n_port))
                        attribs = new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['ports'][n_port]['attributes']
                        fs_path_update(path_node, action=action, attributes=attribs)
                        if action == 'mk':
                            port_action(path_node, ip_attribs, n_port)
        else:
            # Networks
            for n_net in new_nrmap[n_iface]['networks']:
                if not old_nrmap or n_net not in old_nrmap[n_iface]['networks']:
                    utils.log_message('Identified element to {act}: {int}->{net}'.
                                      format(int=n_iface, net=n_net, act=action), 3)
                    path_node = pathlib.Path(path_root, n_iface, n_net.replace('/', '_'))
                    attribs = new_nrmap[n_iface]['networks'][n_net]['attributes']
                    fs_path_update(path_node, action=action, attributes=attribs)

                    # IP addresses in the new network
                    for n_ip in new_nrmap[n_iface]['networks'][n_net]['addresses']:
                        utils.log_message('Identified element to {act}: {int}->{net}->{ip}'.
                                          format(int=n_iface, net=n_net, ip=n_ip, act=action), 3)
                        n_name = new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['attributes']['name']
                        path_node = pathlib.Path(path_root, n_iface, n_net.replace('/', '_'), n_name)
                        attribs = new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['attributes']
                        fs_path_update(path_node, action=action, attributes=attribs)
                else:
                    # IP addresses
                    for n_ip in new_nrmap[n_iface]['networks'][n_net]['addresses']:
                        if not old_nrmap or n_ip not in old_nrmap[n_iface]['networks'][n_net]['addresses']:
                            utils.log_message('Identified element to {act}: {int}->{net}->{ip}'.
                                              format(int=n_iface, net=n_net, ip=n_ip, act=action), 3)
                            ip_attribs = new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['attributes']
                            n_name = ip_attribs['name']
                            path_node = pathlib.Path(path_root, n_iface, n_net.replace('/', '_'), n_name)
                            fs_path_update(path_node, action=action, attributes=ip_attribs)

                            # Open ports of the host
                            for n_port in new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['ports']:
                                path_node = pathlib.Path(path_root, n_iface, n_net.replace('/', '_'),
                                                         n_name, str(n_port))
                                attribs = new_nrmap[n_iface]['networks'][n_net]['addresses'][n_ip]['ports'][n_port]['attributes']
                                fs_path_update(path_node, action=action, attributes=attribs)
                                if action == 'mk':
                                    port_action(path_node, ip_attribs, n_port)


# -- MAIN --------------------------------------------------------------------------------------------------------------
utils.daemonize()

utils.log_message('Starting NR operations', 2)
utils.log_message('Configuration variables: {}'.format(utils.get_nrconfig()), 4)

resource.setrlimit(resource.RLIMIT_NOFILE, (nr_config.nr_max_open_files, nr_config.nr_max_open_files))

# Create nr_root path in advance for the network addresses tree
nr_rootpath = pathlib.Path(nr_config.nr_root)
nr_rootpath.mkdir(mode=nr_config.port_action_filemode, parents=True, exist_ok=True)
shutil.chown(nr_rootpath, user=nr_config.port_action_fileowner, group=nr_config.port_action_filegroup)

nr_map_new = nr_map_old = None
while True:
    nr_map_new = create_nrmap(nr_config.nr_ignore_list)

    # Debug:
    # utils.log_message(json.dumps(nr_map_new, indent=4, sort_keys=True), 3)

    utils.log_message('Checking diff entries: MK round', 3)
    diff_nrmap(nr_map_new, nr_map_old, action='mk')

    if nr_map_old:
        utils.log_message('Checking diff entries: RM round', 3)
        diff_nrmap(nr_map_old, nr_map_new, action='rm')

    nr_map_old = nr_map_new

    utils.log_message('Waiting {} second for the next round'.format(nr_config.nr_scan_interval), 3)
    time.sleep(nr_config.nr_scan_interval)
