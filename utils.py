"""Various utility functions"""

import os
import sys
import signal
import datetime

import nr_config


log_levels = {0: 'None', 1: 'Critical', 2: 'Warning', 3: 'Informational', 4: 'Verbose', 5: 'Debug'}


def log_message(message, message_level=2):
    """Log a message with the timestamp and level"""

    # log_level: 0 - None, 1 - Crit, 2 - Warn, 3 - Info, 4 - Verb, 5 - Debug

    if message_level <= nr_config.nr_log_level:
        print('{tstamp} {lvl} {msg}'.format(tstamp=datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                                            lvl=log_levels[message_level][:4].upper(), msg=message), file=sys.stderr)


def get_nrconfig(cfg_filter=''):
    """Return variables that are defined in the nr_config.py"""

    return [{v: vars(nr_config)[v]} for v in vars(nr_config) if cfg_filter in v and '__' not in v]


def sig_handler(sig, frame):
    """Handle signals received by the program"""

    signame = signal.Signals(sig).name
    if sig == signal.SIGINT or sig == signal.SIGTERM:
        log_message('Stopping operations on signal: {}'.format(signame), 4)
        try:
            os.remove(nr_config.nr_pid_file)
        except OSError as err:
            log_message('Failed to remove the PID-file: {pf} with the error: {e}. You have to do it manually'.
                        format(pf=nr_config.nr_pid_file, e=err), 2)
            sys.exit(1)
        log_message('Operations stopped. See you!'.format(signame), 2)
        sys.exit(0)
    elif sig == signal.SIGHUP:
        log_message('Reload is not implemented yet. You have to kill the process and start it again', 3)
    else:
        log_message('Ignoring unhandled Signal: {}'.format(signame), 3)


def forker():
    """Fork a new process"""

    try:
        if os.fork() > 0:
            # End the parent process
            sys.exit(0)
    except OSError as err:
        log_message('Failed to fork the process: {}'.format(err), 1)
        sys.exit(1)

    return os.getpid()


def daemonize():
    """Become a daemon"""

    try:                                                # Redirect STDERR to the log file
        log_fd = open(nr_config.nr_log_file, 'a')
        os.dup2(log_fd.fileno(), sys.stderr.fileno())
    except IOError as err:
        log_message('Unable to open log file: {lf}: {e}'.format(lf=nr_config.nr_log_file, e=err), 2)

    signal.signal(signal.SIGHUP, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    forker()                                            # Do the first fork()

    os.chdir(os.path.dirname(nr_config.nr_root))
    os.setsid()
    os.umask(0)

    pid = forker()                                      # Do the second fork()

    # Check if NR is running already
    if os.path.isfile(nr_config.nr_pid_file):
        log_message('NR is probably running already!', 2)
        if not nr_config.start_with_force:
            log_message('Unable to start. Check PID-file: {} and proceed accordingly!'.
                        format(nr_config.nr_pid_file), 1)
            sys.exit(1)
        else:
            log_message('Ignoring existing PID-file: {} because NR is started with force'.
                        format(nr_config.nr_pid_file), 2)

    try:
        pid_fd = open(nr_config.nr_pid_file, 'w')
        pid_fd.write(str(pid))
        pid_fd.close()
        log_message('PID-file: {} saved'.format(nr_config.nr_pid_file), 4)
    except IOError as err:
        log_message('Unable to save PID-file: {pf}: {e}. You must MANUALLY check if NR is running already!'.
                    format(pf=nr_config.nr_pid_file, e=err), 2)

    sys.stdin.close()                                   # Finally close STDIN...
    sys.stdout.close()                                  # and STDOUT

    log_message('Daemon started. PID: {}'.format(pid), 3)
