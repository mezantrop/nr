# zeroconf scan based on: https://github.com/jstasiak/python-zeroconf

from zeroconf import ZeroconfServiceTypes, ServiceBrowser, Zeroconf, ServiceStateChange
import socket
import typing
import time
import utils


zcn = {}                                                    # Yeah, it's global variable


def get_zc_netmembers(zeroconf: Zeroconf, service_type: str, name: str, state_change: ServiceStateChange):
    """Get zconf network and members """

    if state_change is ServiceStateChange.Added:
        info = zeroconf.get_service_info(service_type, name)
        if info:
            zc_address = socket.inet_ntoa(typing.cast(bytes, info.address))
            if zc_address not in zcn['addresses']:
                zcn['addresses'][zc_address] = {}
            zcn['addresses'][zc_address]['attributes'] = {}
            zcn['addresses'][zc_address]['attributes']['atime'] = int(time.time())
            zcn['addresses'][zc_address]['attributes']['name'] = info.server

            if 'ports' not in zcn['addresses'][zc_address]:
                zcn['addresses'][zc_address]['ports'] = {}
            portn = typing.cast(int, info.port)
            if portn not in zcn['addresses'][zc_address]['ports']:
                zcn['addresses'][zc_address]['ports'][portn] = {}
            if 'attributes' not in zcn['addresses'][zc_address]['ports'][portn]:
                zcn['addresses'][zc_address]['ports'][portn]['attributes'] = {'atime': int(time.time())}
            if info.properties:
                for key, value in info.properties.items():
                    zcn['addresses'][zc_address]['ports'][portn]['attributes'][key.decode('utf-8')] = value.decode('utf-8')


def get_zc_network():
    zcn.clear()
    zcn['addresses'] = {}
    zcn['attributes'] = {}

    zc_services = ZeroconfServiceTypes.find()

    zeroconf = Zeroconf()

    for zc_service in zc_services:
        ServiceBrowser(zeroconf, zc_service, handlers=[get_zc_netmembers])

    time.sleep(15)
    zeroconf.close()

    utils.log_message('Discovered zeroconf network: {}'.format(zcn), 4)
    # Debug:
    # utils.log_message('Discovered zeroconf network: {}'.format(json.dumps(zcn, indent=4, sort_keys=True)), 4)
    return zcn
