# TODO

- [x] ICMP IPv4/IPv6 ping, unicast/multicast
- [x] Mapping to a file-system  
- [x] TCP port-scanner
- [x] Daemon mode
- [x] Scripting engine for port connections: SSH, FUSE (?), HTTP browser, etc (in progress)
- [x] Zeroconf networks. Bonjour/Avahi implemented.
- [ ] Save selected hosts and credentials (?)
- [ ] Optimize, reformat and clean the code