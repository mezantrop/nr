# Simple TCP port scanner

import socket
import threading
import time

import utils
import nr_config


def check_port(ports, address, port, a_family='AF_INET', p_timeout=nr_config.portsscan_wait_time):
    """Check TCP port. Return: Open - True, Closed - False"""

    family = socket.AF_INET
    if a_family == 'AF_INET6':
        family = socket.AF_INET6

    s = socket.socket(family, socket.SOCK_STREAM)
    ai = socket.getaddrinfo(address, port, family=family, proto=socket.IPPROTO_TCP)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.settimeout(p_timeout)

    if not s.connect_ex(ai[0][4]):
        utils.log_message('{h}: open port detected: {p}'.format(h=address, p=port), 4)
        ports.append(port)


def pscan_ipaddress(address, a_family='AF_INET', p_range=nr_config.portsscan_ports, p_timeout=nr_config.portsscan_wait_time):
    """Scan address and return a list of open TCP ports"""

    port_threads = []
    ports = []

    for port in p_range:
        port_threads.append(threading.Thread(target=check_port, args=(ports, address, port, a_family, p_timeout)))

    # Start threads
    for pth in port_threads:
        pth.start()
        while (len(threading.enumerate())) > nr_config.nr_threads_max:
            utils.log_message('Waiting for threads to finish', 4)
            time.sleep(1)

    # Wait for all threads to finish
    for pth in port_threads:
        pth.join()

    utils.log_message('{h}: open ports: {p}'.format(h=address, p=ports), 3)
    return ports

# pscan_ipaddress('192.168.1.1')