#
# NR - NetRadar - port-actions definitions
#

# NR substitutes these variables:
# {target}  Targe IP address
# {cwd}     Current working directory
# {user}    File owner username
# {group}   File owner group
# {mode}    File-access rights typically to use with chmod command

hp_action = {
    'default': {
        21: {
            'ftp.sh': {
                'file':
                    '#!/bin/sh\n'
                    '\n'
                    'ftp {target}\n'

            },
            'ftpfs.sh': {
                'file':
                    '#!/bin/sh\n'
                    '\n'
                    'ftpfs_root="ftpfs"\n'
                    'mnt_point=`pwd`/"$ftpfs_root"\n'
                    'mkdir -p $mnt_point\n'
                    'curlftpfs {target} "$mnt_point"\n'
            }
        },
        22: {
            'ssh.sh': {
                'file':
                    '#!/bin/sh\n'
                    '\n'
                    'printf "Login: "\n'
                    'read user\n'
                    'ssh "$user"@{target}\n'
            },
            'sshfs.sh': {
                'file':
                    '#!/bin/sh\n'
                    '\n'
                    'sshfs_root="sshfs"\n'
                    'mnt_point=`pwd`/"$sshfs_root"\n'
                    'mkdir -p $mnt_point\n'
                    'read -p "Login: " user\n'
                    'printf "WARNING! MANUAL action required!\n'
                    'Do not forget to umount the remote filesystem: %s mounted on: %s\\n" '
                    '"$user"@{target}:/ $mnt_point\n'
                    'sshfs -o idmap=user "$user"@{target}:/ "$mnt_point"\n'
            }
        },
        23: {
            'telnet.sh': {
                'file':
                    '#!/bin/sh\n'
                    '\n'
                    'telnet {target}\n'
            }
        },
        139: {
            'smbclient': {
                'command':
                    'mnt_pts=`smbutil view -g //{target} | grep Disk | cut -f 1 -d " "`; '
                    'for mpt in $mnt_pts; do '
                    '   mkdir -p {cwd}/$mpt; '
                    '   printf "#!/bin/sh\n" > {cwd}/$mpt/mount.sh; '
                    '   printf "read -p Login: user_name\n" >> {cwd}/$mpt/mount.sh; '                    
                    '   printf "mount_smbfs //\$user_name@{target}/$mpt {cwd}/$mpt" >> {cwd}/$mpt/mount.sh; '
                    '   chown -R {user}:{group} {cwd}/$mpt; '
                    '   chmod -R {mode} {cwd}/$mpt; '
                    'done',
            }
        },
        2049: {
            'nfs': {
                'command':
                    'mnt_pts=`showmount -e {target} | grep -E "^/" | cut -f 1 -d " "`; '
                    'for mpt in $mnt_pts; do '
                    '   mkdir -p {cwd}$mpt; '
                    '   printf "#!/bin/sh\n" > {cwd}$mpt/mount.sh; '
                    '   printf "mount {target}:$mpt {cwd}$mpt" >> {cwd}$mpt/mount.sh; '                    
                    '   chown -R {user}:{group} {cwd}$mpt; '
                    '   chmod -R {mode} {cwd}$mpt; '
                    'done',
            }
        }
    }
}
