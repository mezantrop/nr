from setuptools import setup

setup(
    name='nr.py',
    description='Net Radar - Simple local IP network browser',
    version='0.6',
    url='https://gitlab.com/mezantrop/nr',
    author='Mikhail Zakharov',
    author_email='zmey20000@yahoo.com',
    python_requires='>=3.0.0',
    install_requires=['netifaces', 'zeroconf'],
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: BSD License',
        'Operating System :: MacOS',
        'Operating System :: POSIX :: BSD',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Topic :: System :: Networking',
        'Topic :: System :: Networking :: Monitoring'
    ],
    license='BSD'
)
