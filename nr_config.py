"""Configurable options and defaults"""

nr_root = '/var/nr_root'                                    # NR root network-tree directory

nr_pid_file = '/var/run/nr.pid'                             # Nr saves the running process ID in this file
start_with_force = False                                    # Change this to 'True' to forcible start the daemon

nr_log_file = '/var/log/nr.log'                             # The log filename
nr_log_level = 3                                            # 0 - Log is off, 1 - Crit, 2 - Warn, 3 - Info, 4 - Verb

nr_attributes = '.attributes'                               # A filename to store attributes of the network-tree leaves
nr_max_open_files = 10240                                   # Set maximum open files
nr_virtual_interface = 'NR_VIRTUAL'                         # NR Virtual network interface
nr_virtual_networks = ('ZEROCONF',)
nr_ignore_list = {'interfaces': ('lo0',),                   # Ignored elements
                  'networks': ('127.0.0.0/8',),
                  'addresses': ('127.0.0.1',)}
nr_scan_interval = 300                                      # Interval between network scans in seconds
nr_ping_net_hosts_max = 65536                               # Network with maximum addresses to ping
nr_threads_max = nr_max_open_files / 8                      # Maximum concurrent threads

ping_wait_time = 5                                          # Maximum wait time for echo response in seconds

portsscan_interval = 5                                      # Interval between scanning ports of a host TODO: implement
portsscan_wait_time = 1                                     # Port timeout for port scanner
portsscan_n_min = 1
portsscan_n_max = 4096
# portsscan_ports = [20, 21, 22, 80, 443]
portsscan_ports = [p for p in range(portsscan_n_min, portsscan_n_max)]

port_action_filemode = 0o755                                # File access mode
port_action_fileowner = 'zmey'                              # File owner name. Change it!
port_action_filegroup = 'staff'                             # File owner group. Change it!
