# nr ping routines based on https://gitlab.com/mezantrop/sp_ping

import socket
import struct
import time
import os
import sys

import nr_config
import utils


timeout = 1                                         # Socket timeout in seconds
ping_wait_time = nr_config.ping_wait_time              # Wait time for echo response


def clk_chksum(icmp_packet):
    """Calculate ICMP packet checksum"""

    packet_len = len(icmp_packet)
    summ = 0
    for i in range(0, packet_len, 2):
        if i + 1 < packet_len:
            # Fold 2 neighbour bytes into a number and add it to the summ
            summ += icmp_packet[i] + (icmp_packet[i + 1] << 8)
        else:
            # If there is an odd number of bytes, fake the second byte
            summ += icmp_packet[i] + 0
    # Add carry bit to the sum
    summ = (summ >> 16) + (summ & 0xffff)
    # Truncate to 16 bits and return the checksum
    return ~summ & 0xffff


def addrinfo(address, port, a_family):
    try:
        for ainfo in socket.getaddrinfo(address, port):
            if ainfo[0].name == a_family:
                return ainfo[4]
    except socket.gaierror:
        pass

    utils.log_message('Unable to get {af} address for the {host}'.format(af=a_family, host=address), 2)
    return None


def ping(hosts, target_host, source_host=None, interface=None, a_family='AF_INET', unicast=True, sequence=0, ttl=64):
    """General IPv4/IPv6 ping unicast/broadcast/multicast"""

    if not target_host or a_family != 'AF_INET' and a_family != 'AF_INET6':
        utils.log_message('ping({t}, {s}, {af}): Wrong arguments'.format(t=target_host, s=source_host, af=a_family), 2)
        return hosts

    # Packet header definition
    iphdr_len = 0
    icmphdr_len = 8             # ICMP header length is 8 bytes

    if a_family == 'AF_INET':
        icmp_type_request = 8       # ICMP IPv4 ECHO_REQUEST
        icmp_type_reply = 0         # ICMP IPv4 ECHO_REPLY
    else:
        icmp_type_request = 128     # ICMP IPv6 ECHO_REQUEST
        icmp_type_reply = 129       # ICMP IPv6 ECHO_REPLY

    icmp_code = 0
    icmp_checksum = 0
    icmp_id = os.getpid() & 0xffff  # Generate ID field using PID converted to 16 bit
    # Some ICMP payload examples. Do not make them too long:
    icmp_data = b'\x50\x49\x4E\x47\x2D\x50\x4F\x4E\x47\x20\x46\x52\x4F\x4D' \
                b'\x20\x5A\x4D\x45\x59\x32\x30\x30\x30\x30\x40\x59\x41\x48' \
                b'\x4F\x4F\x2E\x43\x4F\x4D'
    # icmp_data = b'12345678' + b'1234567890' * 4

    data_len = len(icmp_data)

    send_timestamp = time.time()    # Packet creation time
    out_packet = struct.pack('BBHHHQ{}s'.format(data_len), icmp_type_request, icmp_code,
                             icmp_checksum, icmp_id, sequence, int(send_timestamp), icmp_data)
    icmp_checksum = clk_chksum(out_packet)
    out_packet = struct.pack('BBHHHQ{}s'.format(data_len), icmp_type_request, icmp_code,
                             icmp_checksum, icmp_id, sequence, int(send_timestamp), icmp_data)

    # getaddrinfo returns an array of tuples (ainfo) for each address family and socket_kind.
    # We can check AddressFamily.name in ainfo[0] for desired address family AF_INET|AF_INET6 and
    # fetch sockaddr in ainfo[4] to submit it in sendto().
    # sockaddr format IPv6 - (address, port, flow info, scope id)

    sock = None
    try:
        # Sorry guys, non-privileged ICMP does not work on most of OS so:
        if a_family == 'AF_INET':
            # Specify any portnumber (1 for example) in getaddrinfo() just to please the socket library
            target_address = addrinfo(target_host, 1, a_family)
            sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.getprotobyname('icmp'))
            sock.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)                                      # IPv4 TTL
        else:
            if not interface:
                if '%' in target_host:
                    interface = target_host.split('%')[1]
                else:
                    utils.log_message('Unable to identify network interface for IP: {t}'.format(t=target_host), 2)
                    return hosts

            target_address = addrinfo(target_host.split('%')[0] + '%' + interface, 1, a_family)
            sock = socket.socket(socket.AF_INET6, socket.SOCK_RAW, socket.getprotobyname('ipv6-icmp'))
            sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_UNICAST_HOPS, ttl)                     # IPv6 Hops as TTL
    except PermissionError:
        utils.log_message('You must be root to send ICMP packets', 2)
        return hosts
    except:
        utils.log_message('General error creating socket()', 2)
        return hosts

    sock.settimeout(timeout)

    if source_host:
        source_address = addrinfo(source_host, 0, a_family)
        try:
            sock.bind(source_address)
        except OSError:
            utils.log_message('sock.bind({s}): Unable to assign the source address'.format(s=source_host), 2)
            return hosts

    # Send ICMP packet to the target_address formed by sockaddr structure above
    try:
        sock.sendto(out_packet, target_address)
    except socket.error:
        etype, evalue, etrb = sys.exc_info()
        utils.log_message('sock.sendto({t}) failure: {e}'.format(t=target_address[0], e=evalue.args[1]), 2)
        return hosts

    utils.log_message('Ping: {pkt}, {host}'.
                      format(pkt=struct.unpack('BBHHHQ', out_packet[:-data_len]), host=target_address[0]), 4)

    reply = None
    host = None
    while True:
        # Let the buffer_size be the packet maximum size we expect
        buffer_size = 40 + icmphdr_len + struct.calcsize('Q') + data_len     # calcsize('Q'): timestamp size

        try:
            reply, host = sock.recvfrom(buffer_size)
        except socket.timeout:                                              # Nothing found. Are all hosts down?
            utils.log_message('Socket timeout in ping({t}, {s}, {af}) is reached'.
                              format(t=target_host, s=source_host, af=a_family), 4)
            return hosts
        except:
            utils.log_message('ping({t}, {s}, {af}): General error in sock.recvfrom()'.
                              format(t=target_host, s=source_host, af=a_family), 2)
            return hosts

        recv_timestamp = time.time()

        if a_family == 'AF_INET':
            vihl = struct.unpack('B', reply[:1])[0]             # First byte consists of 4 bit IP Version and 4 bit IHL
            ihl = ((vihl << 4) & 0xff) >> 4                     # Cut out the IHL (Internet Header Length) value
            iphdr_len = 4 * ihl                                 # and recalculate IP header length

        # Actual IP packet size of the reply
        packet_size = iphdr_len + icmphdr_len + struct.calcsize("Q") + data_len
        in_packet = struct.unpack('BBHHH', reply[iphdr_len:iphdr_len + icmphdr_len])

        # This is Echo Reply packet (icmp_type_reply in in_packet[0]) to our request with ID (in_packet[3]):
        if in_packet[0] == icmp_type_reply and in_packet[3] == icmp_id:

            if unicast:
                if in_packet[4] == sequence:
                    utils.log_message('{size} bytes from {host}: seq={seq} ttl={ttl} time={time:0.4f} ms'.
                                      format(size=packet_size, host=host[0], seq=in_packet[4], ttl=ttl,
                                             time=(recv_timestamp - send_timestamp) * 1000), 5)
                    utils.log_message('Alive host detected: {h}'.format(h=host[0]), 4)

                    sock.close()
                    hosts.append(host[0])
                    return hosts
            else:
                utils.log_message('{size} bytes from {host}: seq={seq} ttl={ttl} time={time:0.4f} ms'.
                                  format(size=packet_size, host=host[0], seq=in_packet[4], ttl=ttl,
                                         time=(recv_timestamp - send_timestamp) * 1000), 5)
                utils.log_message('Alive host detected: {h}'.format(h=host[0]), 4)
                hosts.append(host[0])
        else:
            if recv_timestamp - send_timestamp > ping_wait_time:
                sock.close()
                return hosts
